﻿/**
 * John Walter Talavera
 * */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Resources;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Collections;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
namespace ResxToCsCode
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input exact Directory where resx is located:");
            string directory = Console.ReadLine();
            Console.WriteLine(CreateCode(directory));
            Console.ReadLine();
        }
        public static string CreateCode(string dir)
        {
            //start of string
            string resources = "#region ResxToCsCode" + Environment.NewLine;
            //delete text file
            System.IO.File.Delete(@".\.ContentsOfResx.txt");
            string unconverted = "";
            //create directory for images
            var allImagesBaseDir = @".\All Images from ";
            try
            {
                using (ResXResourceReader resxReader = new ResXResourceReader(dir))
                {
                    int i = 0;
                    #region Count resx Entries
                    foreach (DictionaryEntry entry in resxReader)
                    {
                        i++;
                    }
                    //output
                    Console.WriteLine("There are " + i + " properties on Resx");
                    i = 0;
                    #endregion
                    //read each resx entry
                    foreach (DictionaryEntry entry in resxReader)
                    {
                        string key = "this." + entry.Key.ToString().Replace(">>", "").Replace("$", "").Replace("this.", "");
                        string valueAsString = entry.Value.ToString().Replace("$", "");
                        string property = key.Split('.')[key.Split('.').Length - 1];
                        var valueType = entry.Value.GetType().FullName;
                        //append to txt every loop iteration
                        System.IO.File.AppendAllText(@".\.ContentsOfResx.txt", key.ToString() + " = " + valueAsString.ToString() + Environment.NewLine);
                        #region for size and point
                        if (entry.Value is Size
                         || entry.Value is SizeF
                         || entry.Value is Point)
                        {
                            List<int> number = new List<int>();
                            var entrySplit = valueAsString.ToString().Split(',');
                            foreach (var each in entrySplit)
                            {
                                //get number from each entry
                                number.Add(int.Parse(Regex.Match(each, @"\d+").Value));
                            }

                            resources += key + "=" + "new " + valueType + "(" + number[0] + ", " + number[1] + ");" + Environment.NewLine;
                            i++;
                        }
                        #endregion
                        #region for enum
                        else if (entry.Value is FlatStyle
                          || entry.Value is AutoSizeMode
                          || entry.Value is DockStyle
                          || entry.Value is Color
                          || entry.Value is ImeMode
                          || entry.Value is BorderStyle
                          || entry.Value is HorizontalAlignment)
                        {
                            resources += key + "=" + valueType + "." + entry.Value.ToString() + ";" + Environment.NewLine;

                            i++;
                        }
                        #endregion
                        #region for anchor for contentalignment
                        else if (entry.Value is AnchorStyles
                          || entry.Value is ContentAlignment)
                        {
                            List<string> direction = new List<string>();
                            var entrysplit = valueAsString.Split(',');
                            foreach (var q in entrysplit)
                            {
                                direction.Add(valueType + "." + q.Trim());
                            }
                            resources += key + "=(" + string.Join("|", direction.ToArray()) + ");" + Environment.NewLine;
                            i++;
                        }
                        #endregion
                        #region for padding
                        else if (property.Equals("Margin")
                          || property.Equals("Padding"))
                        {
                            List<string> direction = new List<string>();
                            var entrysplit = valueAsString.Split(',');
                            foreach (var q in entrysplit)
                            {
                                direction.Add(q.Trim('{', '}').Split('=')[1]);
                            }
                            resources += key + "= new Padding(" + string.Join(",", direction.ToArray()) + ");" + Environment.NewLine;
                            i++;
                        }
                        #endregion
                        #region for font
                        else if (entry.Value is Font)
                        {
                            List<string> prop = new List<string>();
                            var entrysplit = valueAsString.Split(',');
                            foreach (var q in entrysplit)
                            {
                                prop.Add(q.Trim('[', ']'));
                            }
                            int graphicsUnit = int.Parse(prop[2].Split('=')[1]);
                            string gu = "";
                            switch (graphicsUnit)
                            {
                                case 0:
                                    gu = "World";
                                    break;
                                case 1:
                                    gu = "Display";
                                    break;
                                case 2:
                                    gu = "Pixel";
                                    break;
                                case 3:
                                    gu = "Point";
                                    break;
                                case 4:
                                    gu = "Inch";
                                    break;
                                case 5:
                                    gu = "Document";
                                    break;
                                case 6:
                                    gu = "Millimeter";
                                    break;
                            }
                            resources += key + "=new " + valueType + "(\"" + prop[0].Split('=')[1] + "\", " + prop[1].Split('=')[1] + "f, FontStyle.Regular ,GraphicsUnit." + gu + ", " + prop[3].Split('=')[1] + ", " + prop[4].Split('=')[1].ToLower() + ");" + Environment.NewLine;
                            i++;
                        }
                        #endregion
                        #region for zorder
                        else if (property.Equals("ZOrder"))
                        {
                            resources += "this.Controls.SetChildIndex(this." + key.Split('.')[1] + ", " + valueAsString.ToString() + ");" + Environment.NewLine;
                        }
                        #endregion
                        #region for bool
                        else if (entry.Value is bool)
                        {
                            resources += key + "=" + valueAsString.ToString().ToLower() + ";" + Environment.NewLine;
                            i++;
                        }
                        #endregion
                        #region for as is for string
                        else if (property.Equals("TabIndex")
                          || property.Equals("TitleHeight")
                          || property.Equals("Parent")
                          || property.Equals("ItemHeight")
                          || property.Equals("ColumnCount")
                          || property.Equals("RowCount")
                          || property.Equals("LayoutSettings")
                          || property.Equals("Width")
                          || property.Equals("MaxLength")
                          || property.Equals("ColumnWidthList")
                          || entry.Value is int
                          || entry.Value is decimal
                          || entry.Value is float
                          || (entry.Value is string && !property.Equals("Type")))
                        {
                            if (entry.Value is string && !property.Equals("Parent"))
                            {
                                resources += key + "=\"" + valueAsString + "\";" + Environment.NewLine;
                            }
                            else
                            {
                                resources += key + "=" + valueAsString + ";" + Environment.NewLine;
                            }
                            i++;
                        }
                        #endregion
                        #region for image resources
                        //else if (entry.Value is Image || entry.Value is Icon || entry.Value is ImageListStreamer)
                        //{
                        //string control = entry.Key.ToString().Split('.')[0];
                        //  var directoryOfImages = allImagesBaseDir + key;
                        //  //create another directory
                        //  if (!Directory.Exists(directoryOfImages))
                        //    Directory.CreateDirectory(directoryOfImages);
                        //  Image img = entry.Value as Image;
                        //  if (img != null)
                        //  {
                        //    var extension = GetExtension(img.RawFormat);

                        //    var fileName = string.Format("Image_{0}{1}", imageIndex.ToString("00000"), extension);
                        //    img.Save(Path.Combine(directoryOfImages, fileName), img.RawFormat);
                        //    resources += control + ".Images.Add(Image.FromFile(@\"" + fileName + "\"));\r\n";
                        //  }
                        //  Icon ico = entry.Value as Icon;
                        //  if (ico != null)
                        //  {
                        //    var extension = ".ico";
                        //    Bitmap bmp = ico.ToBitmap();

                        //    var fileName = string.Format("Image_{0}{1}", imageIndex.ToString("00000"), extension);
                        //    bmp.Save(Path.Combine(directoryOfImages, fileName), ImageFormat.Icon);
                        //    resources += control + ".Images.Add(Image.FromFile(@\"" + fileName + "\"));\r\n";
                        //  }

                        //  ImageListStreamer ils = entry.Value as ImageListStreamer;
                        //  if (ils != null)
                        //  {
                        //    ImageList imageList1 = new ImageList();
                        //    imageList1.ImageStream = ils;
                        //    if (imageList1 != null && imageList1.Images != null && imageList1.Images.Count > 0)
                        //    {
                        //      for (int z = 0; z < imageList1.Images.Count; z++, imageIndex++)
                        //      {
                        //        var image = imageList1.Images[z];
                        //        var extension = GetExtension(image.RawFormat);

                        //        var filename = string.Format("Image_{0}{1}", imageIndex.ToString("00000"), extension);
                        //        image.Save(Path.Combine(directoryOfImages, filename), image.RawFormat);

                        //      }
                        //      //create script
                        //      string[] directories = Directory.GetFiles(directoryOfImages);
                        //      var files = new DirectoryInfo(directoryOfImages).GetFiles("*.*");
                        //      var index = 0;
                        //      foreach (string path in directories)
                        //      {
                        //        resources += control + ".Images.Add(Image.FromFile(@\"" + path + "\"));\r\n";
                        //        resources += control + ".Images.SetKeyName(" + index + ", \"" + files[index].Name + "\");\r\n";
                        //        index++;
                        //      }
                        //    }
                        //  }

                        //}
                        #endregion
                        else
                        {
                            unconverted += key.ToString() + " = " + valueAsString.ToString() + Environment.NewLine;
                        }
                    }

                    Console.WriteLine("There are " + i + " properties converted to c# code");
                }
                resources += "#endregion" + Environment.NewLine;
                Console.WriteLine("ConvertedToCode.txt contains resx strings converted to C# codes.");
                Console.WriteLine("UnconvertedToCode.txt contains all key/value pair of the resx unconverted yet.");
                Console.WriteLine("ContentsofResx.txt contains resx all key/value pair of the resx.");
                System.IO.File.WriteAllText(@".\.ConvertedToCode.txt", resources);
                System.IO.File.WriteAllText(@".\.UnconvertedToCode.txt", unconverted);

                return "successful conversion!";

            }
            catch (Exception e)
            {

                return "Could not find resx file, Unsuccessful conversion! : " + e.Message;
            }
        }











        private static string GetExtension(ImageFormat imageFormat)
        {
            if (ImageFormat.Bmp.Guid == imageFormat.Guid)
                return ".bmp";
            if (ImageFormat.Emf.Guid == imageFormat.Guid)
                return ".emf";
            if (ImageFormat.Exif.Guid == imageFormat.Guid)
                return ".exif";
            if (ImageFormat.Gif.Guid == imageFormat.Guid)
                return ".gif";
            if (ImageFormat.Icon.Guid == imageFormat.Guid)
                return ".ico";
            if (ImageFormat.Jpeg.Guid == imageFormat.Guid)
                return ".jpeg";
            if (ImageFormat.MemoryBmp.Guid == imageFormat.Guid)
                return ".bmp";
            if (ImageFormat.Png.Guid == imageFormat.Guid)
                return ".png";
            if (ImageFormat.Tiff.Guid == imageFormat.Guid)
                return ".tiff";
            if (ImageFormat.Wmf.Guid == imageFormat.Guid)
                return ".wmf";
            return ".bmp";
        }
    }

}
